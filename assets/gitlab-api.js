/* eslint-disable import/no-amd, import/no-dynamic-require */
require(['gitbook'], (gitbook) => {
  const localStorageKey = key => `gitlab-api-${key}`;

  const randomHex = (length) => {
    if (length > 0) {
      return Math.round(Math.random() * 15).toString(16) + randomHex(length - 1);
    }

    return '';
  };

  class GitLabApi {
    constructor(host, apiVersion, applicationId) {
      this.applicationId = applicationId;
      this.baseUrl = `${host}/api/${apiVersion}`;
      this.host = host;

      ['accessToken', 'currentUser', 'oauthState'].forEach((key) => {
        const storedValue = localStorage[localStorageKey(key)];
        if (storedValue) {
          this[`_${key}`] = JSON.parse(storedValue);
        }

        Object.defineProperty(this, key, {
          get: () => this[`_${key}`],
          set: (value) => {
            if (value) {
              localStorage[localStorageKey(key)] = JSON.stringify(value);
            } else {
              localStorage.removeItem(localStorageKey(key));
            }

            this[`_${key}`] = value;
            return value;
          },
        });
      });
    }

    fetch(endpoint, options) {
      if (!this.accessToken) {
        return Promise.reject(new Error('You need to call login() first!'));
      }

      const headers = new Headers();
      headers.append('authorization', `Bearer ${this.accessToken}`);

      return fetch(this.baseUrl + endpoint, Object.assign({ headers }, options))
        .then((response) => {
          if (!response.ok) {
            return response.json()
              .then((data) => {
                const error = new Error(data.error || data.message);
                error.status = response.status;
                throw error;
              });
          }

          return response;
        });
    }

    fetchCurrentUser() {
      this.currentUser = null;

      return this.fetchJson('/user')
        .then((user) => {
          this.currentUser = user;
          return user;
        })
        .catch((error) => {
          this.accessToken = null;
          throw error;
        });
    }

    fetchJson(endpoint, options) {
      return this.fetch(endpoint, options)
        .then(response => response.json());
    }

    login() {
      if (location.hash) {
        const oauthState = /state=([^&]+)/.exec(location.hash)[1];

        if (oauthState !== this.oauthState) {
          throw new Error('OAuth state mismatch!');
        }

        this.accessToken = /access_token=([^&]+)/.exec(location.hash)[1];
      }

      if (this.accessToken) {
        return this.fetchCurrentUser();
      }

      const currentPage = location.href;
      this.oauthState = randomHex(64);
      location.href = `${this.host}/oauth/authorize?client_id=${this.applicationId}&redirect_uri=${currentPage}&response_type=token&state=${this.oauthState}`;

      // do not resolve/reject, wait for location.href to change
      return new Promise(() => {});
    }
  }

  gitbook.events.on('start', (event, config) => {
    const pluginConfig = config['gitlab-api'];
    gitbook.gitlabApi = new GitLabApi( // eslint-disable-line no-param-reassign
      pluginConfig.host,
      pluginConfig.apiVersion,
      pluginConfig.applicationId,
    );
  });
});
