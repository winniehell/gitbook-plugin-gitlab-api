# GitLab API Example

See it happening on https://winniehell.de/gitbook-plugin-gitlab-api/.

<div id="dynamic-content"><i class="fa fa-spin fa-spinner"></i></div>

<script>
  // wait for GitBook to be loaded
  document.addEventListener('DOMContentLoaded', (event) => {
    const dynamicContentElement = document.querySelector('#dynamic-content');
    const spinner = dynamicContentElement.querySelector('.fa-spinner');

    const projectsDiv = document.createElement('div');
    projectsDiv.innerHTML = '<h3>Some randomly selected projects</h3>'

    // wait for GitBook plugins to be loaded
    gitbook.events.on('page.change', () => {
      gitbook.gitlabApi.login()
        .then((user) => {
          const userDiv = document.createElement('div');
          userDiv.innerHTML = `You are now logged in as <a href="${user.web_url}">${user.username}</a>.`;
          dynamicContentElement.insertBefore(userDiv, spinner);

          dynamicContentElement.insertBefore(projectsDiv, spinner);

          return gitbook.gitlabApi.fetchJson('/projects');
        })
        .then((projects) => {
          projects.forEach((project) => {
            const rowDiv = document.createElement('div');
            const owner = project.namespace;
            rowDiv.innerHTML = `<a href="${project.web_url}">${project.name}</a> owned by <a href="${owner.web_url}">${owner.name}</a>`
            projectsDiv.appendChild(rowDiv);
          });

          spinner.remove();
        });
    });
  });
</script>

